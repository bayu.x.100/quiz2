MariaDB [quiz2]> select customers.name as "customer_name", sum(orders.amount) as "total_amount"
    -> from customers
    -> join orders
    -> on customers.id = orders.customer_id
    -> group by customers.name
    -> order by sum(orders.amount) asc;
+---------------+--------------+
| customer_name | total_amount |
+---------------+--------------+
| John Doe      |          750 |
| Jane Doe      |         1350 |
+---------------+--------------+
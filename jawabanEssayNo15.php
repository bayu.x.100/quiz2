<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php

function hitung ($string_data) {
    $pisah = preg_split('~(?:\d+\.\d+|\w+|\S)\K *~', $string_data, null, PREG_SPLIT_NO_EMPTY);
    $angka1 = (float)$pisah[0];
    $angka2 = (float)$pisah[2];
    $operator = $pisah[1];
    if ($operator == '*') {
        $hasil = $angka1 * $angka2;
    }
    elseif ($operator == '+') {
        $hasil = $angka1 + $angka2;
    }
    elseif ($operator == ':') {
        $hasil = $angka1 / $angka2;
    }
    elseif ($operator == '-') {
        $hasil = $angka1 - $angka2;
    }
    elseif ($operator == '%') {
        $hasil = $angka1 % $angka2;
    }
    echo $hasil."<br>";
}

    echo hitung("102*2"); //204
    echo hitung("2+3"); //5
    echo hitung("100:25"); //4
    echo hitung("10%2"); //0
    echo hitung("99-2"); //97


?>
</body>
</html>
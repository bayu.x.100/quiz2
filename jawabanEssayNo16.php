//Tambah Data Tabel customers

MariaDB [quiz2]> insert into customers values (
    -> '',
    -> 'John Doe',
    -> 'john@doe.com',
    -> 'john123'
    -> );

MariaDB [quiz2]> insert into customers values (
    -> '',
    -> 'Jane Doe',
    -> 'jane@doe.com',
    -> 'jenita123'
    -> );

MariaDB [quiz2]> select * from customers;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+

//Tambah Data Tabel orders

MariaDB [quiz2]> insert into orders values (
    -> '',
    -> 500,
    -> 1
    -> );

MariaDB [quiz2]> insert into orders values (
    -> '',
    -> 200,
    -> 2
    -> );

MariaDB [quiz2]> insert into orders values (
    -> '',
    -> 750,
    -> 2
    -> );

MariaDB [quiz2]> insert into orders values (
    -> '',
    -> 250,
    -> 1
    -> );

MariaDB [quiz2]> insert into orders values (
    -> '',
    -> 400,
    -> 2
    -> );

MariaDB [quiz2]> select * from orders;
+----+--------+-------------+
| id | amount | customer_id |
+----+--------+-------------+
|  1 | 500    |           1 |
|  2 | 200    |           2 |
|  3 | 750    |           2 |
|  4 | 250    |           1 |
|  5 | 400    |           2 |
+----+--------+-------------+